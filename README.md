# mdbook-bib



## Getting started

- [Install Rust](https://www.rust-lang.org/tools/install)
```bash
curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh
```
- compile
```
cargo build
```
- Install mdbook
```
cargo install --path .
```