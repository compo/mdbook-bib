extern crate lazy_static;


use std::fs;
use std::io::Read;
use std::path::Path;
use std::path::PathBuf;

use log;

use anyhow::anyhow;
use reqwest::blocking::Response;

use mdbook::preprocess::{Preprocessor, PreprocessorContext};
use mdbook::book::{Book, Chapter};
use mdbook::errors::{Error, Result as MdResult};


use biblatex::Bibliography;

use crate::bibliography::ExpandRefs;
use crate::config::Config;
use crate::file_utils;



static NAME: &str = "bib";

pub struct BiblioPreprocessor;

impl Default for BiblioPreprocessor {
    fn default() -> BiblioPreprocessor {
        BiblioPreprocessor
    }
}


impl Preprocessor for BiblioPreprocessor {
  fn name(&self) -> &str {
      NAME
  }

  fn run(&self, ctx: &PreprocessorContext, mut book: Book) -> Result<Book, anyhow::Error> {
      log::info!("Processor Name: {name}", name = self.name());

      let book_src_root = ctx.root.join(ctx.config.book.src.to_owned());
      let table = ctx.config.get_preprocessor(self.name());

      let config = match Config::build_from(table, book_src_root) {
        Ok(config) => config, 
        Err(err) => {
            log::warn!("Error reading configuration. Exiting!");
            return Err(err);
        }
      };

      let bib_content = match retrieve_bibliography_content(ctx, &config) {
          Ok(val) => val, 
          Err(err) => {
            log::warn!("Raw Bibliography content couldn't be retrieved. Exiting!");
            return Err(err);
          }
      };
      
      let bibliography = match Bibliography::parse(bib_content.as_str()){
        Ok(val) => val, 
        Err(err) => {
            log::warn!( "Error building Bibliography from raw content. Exiting!");
            return Err(anyhow!("{err}", err = err));
        }
      };

      let cited = bibliography.expand_cite(
        &mut book,
        &config.bib_hb_html
    );

      let bib_content_html = bibliography.to_html(
          &cited,
          config.cited_only,
          &config.bib_hb_html,
      );


    let html_content = format!("# {}\n{}\n{}\n{}",
        config.title, 
        config.js_html,
        config.css_html,
        bib_content_html
    );

    let bib_chapter = Chapter::new(
        &config.title, 
        html_content,
        PathBuf::from("bibliography.md"),
        Vec::new(),
    );

    book.push_item(bib_chapter);

    Ok(book)
  }

  fn supports_renderer(&self, renderer: &str) -> bool {
      renderer != "not-supported"
  }
}



fn retrieve_bibliography_content(
    ctx: &PreprocessorContext,
    cfg: &Config,
) -> Result<String, Error> {
    let bib_content = match &cfg.bibliography {
        Some(biblio_file) => {
            log::info!("Bibliography file: {biblio_file}");
            let mut biblio_path = ctx.root.join(ctx.config.book.src.to_owned());
            biblio_path = biblio_path.join(Path::new(&biblio_file));
            if !biblio_path.exists() {
                Err(anyhow!("Bibliography file {path} not found!", path = biblio_path.display()))
            } else {
                log::info!("Bibliography path: {path}", path = biblio_path.display());
                load_bibliography(biblio_path)
            }
        }
        _ => {
            log::warn!("Bibliography file not specified. Trying download from Zotero");
            match &cfg.zotero_uid {
                Some(uid) => {
                    let user_id = uid.to_string();
                    let bib_str = download_bib_from_zotero(user_id).unwrap_or_default();
                    if !bib_str.is_empty() {
                        let biblio_path = ctx.root.join(Path::new("my_zotero.bib"));
                        log::info!("Saving Zotero bibliography to {:?}", biblio_path);
                        let _ = fs::write(biblio_path, bib_str.to_owned());
                        Ok(bib_str)
                    } else {
                        // warn!("Bib content retrieved from Zotero is empty!");
                        Err(anyhow!("Bib content retrieved from Zotero is empty!"))
                    }
                }
                _ => Err(anyhow!("Zotero user id not specified either :(")),
            }
        }
    };
    bib_content
}

/// Load bibliography from file
fn load_bibliography<P: AsRef<Path>>(biblio_file: P) -> MdResult<String> {
    log::info!("Loading bibliography from {:?}...", biblio_file.as_ref());

    let biblio_file_ext = file_utils::get_filename_extension(biblio_file.as_ref());
    if biblio_file_ext.unwrap_or_default().to_lowercase() != "bib" {
        log::warn!(
            "Only biblatex-based bibliography is supported for now! Yours: {:?}",
            biblio_file.as_ref()
        );
        return Ok("".to_string());
    }
    Ok(fs::read_to_string(biblio_file)?)
}

fn extract_biblio_data_and_link_info(res: &mut Response) -> (String, String) {
    let mut biblio_chunk = String::new();
    let _ = res.read_to_string(&mut biblio_chunk);
    let link_info_in_header = res.headers().get("link");
    log::debug!("Header Link content: {:?}", link_info_in_header);
    let link_info_as_str = link_info_in_header.unwrap().to_str();

    (link_info_as_str.unwrap().to_string(), biblio_chunk)
}

/// Download bibliography from Zotero
fn download_bib_from_zotero(user_id: String) -> MdResult<String, Error> {
    let mut url = format!("https://api.zotero.org/users/{}/items?format=biblatex&style=biblatex&limit=100&sort=creator&v=3", user_id);
    log::info!("Zotero's URL biblio source:\n{:?}", url);
    let mut res = reqwest::blocking::get(&url)?;
    if res.status().is_client_error() || res.status().is_client_error() {
        Err(anyhow!(format!(
            "Error accessing Zotero API {:?}",
            res.error_for_status()
        )))
    } else {
        let (mut link_str, mut bib_content) = extract_biblio_data_and_link_info(&mut res);
        while link_str.contains("next") {
            // Extract next chunk URL
            let next_idx = link_str.find("rel=\"next\"").unwrap();
            let end_bytes = next_idx - 3; // The > of the "next" link is 3 chars before rel=\"next\" pattern
            let slice = &link_str[..end_bytes];
            let start_bytes = slice.rfind('<').unwrap_or(0);
            url = link_str[(start_bytes + 1)..end_bytes].to_string();
            log::info!("Next biblio chunk URL:\n{:?}", url);
            res = reqwest::blocking::get(&url)?;
            let (new_link_str, new_bib_part) = extract_biblio_data_and_link_info(&mut res);
            link_str = new_link_str;
            bib_content.push_str(&new_bib_part);
        }
        Ok(bib_content)
    }
}