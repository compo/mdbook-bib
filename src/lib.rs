#[macro_use]
extern crate lazy_static;

// mod config;
mod config;
mod file_utils;
mod placeholder;
mod bibliography;
pub mod preprocess;
mod type_conversion;
