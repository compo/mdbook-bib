use biblatex::{Chunk, DateValue, Datetime, Entry, Spanned, RetrievalError};

pub(crate) fn get_first_author(entry: &Entry) -> String {
  let mut author_string = "".to_string();
  match  entry.author() {
    Ok(raw_authors_list) => {
      if let Some(author) = raw_authors_list.get(0) {
        let current_author_string = format!("{}", author.name);
        author_string.push_str(current_author_string.trim_end());
        if raw_authors_list.len() > 1 {
          author_string.push_str(" et al.");
        }
      }
    }, 
    Err(_) => ()
  };
  author_string
}

pub(crate) fn get_authors_long(entry: &Entry) -> String {
  let mut author_string = "".to_string();
  match  entry.author() {
    Ok(raw_authors_list) => {
      let mut authors_list = raw_authors_list.iter();
      let mut last_author = false;

      if raw_authors_list.len() > 4 {
        authors_list = raw_authors_list[0..2].iter();
        last_author = true;
      }

      for author in authors_list {
        let current_author_string = format!(
          "{} {} {} {}",
          author.prefix, 
          author.name, 
          author.given_name, 
          author.suffix
        );
        if !author_string.is_empty() {
          author_string.push_str(", ");
        }
        author_string.push_str(current_author_string.trim_end());
      }

      if last_author {
        let author = raw_authors_list.last().unwrap();
        let last_author_string = format!(
          ", ... {} {} {} {}",
          author.prefix, 
          author.name, 
          author.given_name, 
          author.suffix
        );
        author_string.push_str(last_author_string.trim_end());
      }
    }, 
    Err(_) => ()
  }
  author_string
}

pub(crate) fn get_authors_short(entry: &Entry) -> String {
  let mut author_string = "".to_string();
  match  entry.author() {
    Ok(raw_authors_list) => {
      let first_author = &raw_authors_list[0];
      author_string.push_str(format!("{}", first_author.name).trim_end());
    }, 
    Err(_) => ()
  };
  author_string
}


fn datetime_year_to_string(date: Datetime) -> String {
  format!("{}", date.year)
}

fn datetime_month_to_string(date: Datetime) -> String {
  if let Some(month) = date.month {
    format!("({})", month_abbr(month))
  } else {
    "".to_string()
  }
}

pub(crate) fn get_date_year(entry: &Entry) -> String {
  let mut date_string = "".to_string();
  match entry.date() {
    Ok(date) => {
      match date.value {
        DateValue::At(date) => {
            date_string.push_str(datetime_year_to_string(date).as_str());
        },
        DateValue::After(date) => {
            date_string.push_str(datetime_year_to_string(date).as_str());
            date_string.push_str(" --");
        },
        DateValue::Before(date) => {
            date_string.push_str("-- ");
            date_string.push_str(datetime_year_to_string(date).as_str());
        },
        DateValue::Between(start, end) => {
            date_string.push_str(datetime_year_to_string(start).as_str());
            date_string.push_str(" -- ");
            date_string.push_str(datetime_year_to_string(end).as_str());
        },
      }
    }, 
    Err(_) => ()
  }

  date_string
}

pub(crate) fn get_date_month(entry: &Entry) -> String {
  let mut date_string = "".to_string();
  match entry.date() {
    Ok(date) => {
      match date.value {
        DateValue::At(date) => {
            date_string.push_str(datetime_month_to_string(date).as_str());
        },
        DateValue::After(date) => {
            date_string.push_str(datetime_month_to_string(date).as_str());
            date_string.push_str(" --");
        },
        DateValue::Before(date) => {
            date_string.push_str("-- ");
            date_string.push_str(datetime_month_to_string(date).as_str());
        },
        DateValue::Between(start, end) => {
            date_string.push_str(datetime_month_to_string(start).as_str());
            date_string.push_str(" -- ");
            date_string.push_str(datetime_month_to_string(end).as_str());
        },
      }
    }, 
    Err(_) => ()
  }

  date_string
}

pub(crate) fn extract_record(record: Result<&[Spanned<Chunk>], RetrievalError>) -> Option<String> {
  let mut record_string = "".to_string();
  match record {
    Ok(chunk_vect) => {
        for chunk in chunk_vect {
          record_string.push_str(chunk.as_ref().v.get()); 
        }
        Some(record_string)
    },
    Err(_) => None
  } 
}


fn month_abbr(month: u8) -> &'static str {
  match month {
    0 => "jan",
    1 => "feb",
    2 => "mar",
    3 => "apr",
    4 => "may",
    5 => "jun",
    6 => "jul",
    7 => "aug",
    8=> "sep",
    9=> "oct",
    10 => "nov",
    11 => "dec",
    _ => ""
  } 
}

pub(crate) fn get_journal(entry: &Entry) -> String {
  let mut journal = "".to_string();
  if let Some(short_name) = extract_record(entry.short_journal()) {
    journal.push_str(short_name.as_str());
  } else if let Some(long_name) = extract_record(entry.journal()) {
    journal.push_str(long_name.as_str());
  }
  journal
}