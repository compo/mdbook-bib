use std::ffi::OsStr;
use std::path::Path;
use std::path::Component;


/// Get filename extension
pub fn get_filename_extension(filename: &Path) -> Option<&str> {
    filename.extension().and_then(OsStr::to_str)
}

pub fn breadcrumbs_up_to_root(source_file: &std::path::Path) -> String {
    if source_file.as_os_str().is_empty() {
        return "".into();
    }

    let components_count = source_file.components().fold(0, |acc, c| match c {
        Component::Normal(_) => acc + 1,
        Component::ParentDir => acc - 1,
        Component::CurDir => acc,
        Component::RootDir | Component::Prefix(_) => panic!(
            "mdBook is not supposed to give us absolute paths, only relative from the book root."
        ),
    }) - 1;

    let mut to_root = vec![".."; components_count].join("/");
    if components_count > 0 {
        to_root.push('/');
    }

    to_root
}