use mdbook::book::Book;
use std::collections::HashSet;

pub trait ExpandRefs {
    fn to_html(
      self,
      cited: &HashSet<String>,
      cited_only: bool,
      references_tpl: &String,
  ) -> String;
  fn expand_cite(&self, bool: &mut Book, references_tpl: &String) -> HashSet<String>;
}