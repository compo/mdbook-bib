extern crate lazy_static;

// use std::cell::Ref;
use std::collections::{HashSet};
use itertools::Itertools;
// use itertools::merge;

// use crate::config::Config;
use handlebars::Handlebars;
use log;
use mdbook::book::{Book, BookItem, Chapter};


use biblatex::{Bibliography, Entry};
use serde::{Deserialize, Serialize};

use crate::placeholder::*;
// use crate::placeholder;
use crate::type_conversion::*;
use crate::file_utils::*;

mod traits;
pub use traits::ExpandRefs as ExpandRefs;


impl  ExpandRefs for Bibliography {
    // Get references and info from the bibliography file specified in the config
    // arg "bibliography" or in Zotero
    fn to_html(
        self,
        cited: &HashSet<String>,
        cited_only: bool,
        references_tpl: &String,
    ) -> String {
        let mut handlebars = Handlebars::new();
        handlebars
            .register_template_string("references", references_tpl)
            .unwrap();
        // log::debug!("Handlebars content: {:?}", handlebars);

        let mut content: String = String::from("");
        for entry in self {
            if !cited_only || cited.contains(&entry.key) {
                let value = BibItem::from_bib_entry(&entry);
                content.push_str(handlebars.render("references", &value).unwrap().as_str());
            }
        }

        log::debug!("Generated Bib Content: {:?}", content);
        content
    }

    fn expand_cite(&self, book: &mut Book, references_tpl: &String) -> HashSet<String> {
        let mut cited = HashSet::new();
        book.for_each_mut(|section: &mut BookItem| {
            if let BookItem::Chapter(ref mut ch) = *section {
                if let Some(_) = ch.path {
                    let new_content = replace_all_placeholders(
                        self,
                        ch,
                        &mut cited,
                        references_tpl
                    );
                    ch.content = new_content;
                }
            }
        });
        cited
    }
}
    
fn replace_all_placeholders(
    biblio: &Bibliography, 
    chapter: &Chapter,
    cited: &mut HashSet<String>,
    references_tpl: &String
) ->  String {
    // When replacing one thing in a string by something with a different length,
    // the indices after that will not correspond,
    // we therefore have to store the difference to correct this
    let mut previous_end_index = 0;
    let mut replaced = String::new();
    
    let chapter_path = chapter
        .path
        .as_deref()
        .unwrap_or_else(|| std::path::Path::new(""));

    let placeholder_iter = find_placeholders(&chapter.content)
        .merge(find_at_placeholders(&chapter.content))
        .merge(find_double_dollar_placeholders(&chapter.content))
        .merge(find_dollar_placeholders(&chapter.content));
    
    for placeholder in placeholder_iter {
        replaced.push_str(&chapter.content[previous_end_index..placeholder.start_index]);
        replaced.push_str(&placeholder.render(
            breadcrumbs_up_to_root(chapter_path),
            biblio,
            &references_tpl
        ));
        previous_end_index = placeholder.end_index;
    
        match placeholder.placeholder_type {
            PlaceholderType::Cite(ref cite) | PlaceholderType::AtCite(ref cite)=> {
                cited.insert(cite.to_owned());
            }, 
            _ => {}
        }
    }

    replaced.push_str(&chapter.content[previous_end_index..]);
    replaced
}



/// Bibliography item representation.
/// TODO: Complete with more fields when necessary
#[derive(Debug, Clone, Default, PartialEq, Serialize, Deserialize)]
pub struct BibItem {
    /// The citation key.
    pub citation_key: String,
    /// The article's title.
    pub title: String,
    /// The first author
    pub author_first: String,
    /// The article's author/s.
    pub authors_long: String,
    /// The article's author/s.
    pub authors_short: String,
    /// Pub month.
    pub pub_month: String,
    /// Pub year.
    pub pub_year: String,
    /// The Journal/ publisher
    pub journal: String,
    /// Summary/Abstract.
    pub summary: String,
    /// The article's url.
    pub url: Option<String>,
}

impl BibItem {
    /// Create a new bib item with the provided content.
    pub fn from_bib_entry(entry: &Entry) -> BibItem {
        BibItem {
            citation_key: entry.key.to_owned(),
            title: extract_record(entry.title()).unwrap_or_default(),
            author_first: get_first_author(entry),
            authors_long: get_authors_long(entry),
            authors_short: get_authors_short(entry),
            pub_month: get_date_month(entry),
            pub_year: get_date_year(entry),
            journal: get_journal(entry),
            summary: extract_record(entry.abstract_()).unwrap_or_default(),
            url: match entry.doi() {
                Ok(doi) => Some(format!("https://www.doi.org/{}", doi.trim())),
                Err(_) => match entry.doi() {
                    Ok(url) => Some(url), 
                    Err(_) => None
                }
            },
        }
    }

    pub(crate) fn get_citation(&self) -> String {
        let mut ref_sring = "".to_string();

        if !self.author_first.is_empty() {
            ref_sring.push_str(self.author_first.as_str());
        }

        if !self.journal.is_empty() {
            if !ref_sring.is_empty() {
                ref_sring.push_str(", ");
            }
            ref_sring.push_str("*");
            ref_sring.push_str(self.journal.as_str());
            ref_sring.push_str("* ");
        }

        if !self.pub_year.is_empty() {
            if !ref_sring.is_empty() {
                ref_sring.push_str(", ");
            }
            ref_sring.push_str(self.pub_year.as_str());
        }

        ref_sring
    }
}
