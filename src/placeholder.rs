use biblatex::Bibliography;
use std::cmp::Ordering;
use fancy_regex::Regex;
use fancy_regex::CaptureMatches;
use fancy_regex::Captures;
use handlebars::Handlebars;


use crate::bibliography::BibItem;


fn parse_dollar(cite: &str) -> PlaceholderType {
  PlaceholderType::DollarMaths(cite.to_owned())
}

fn parse_double_dollar(cite: &str) -> PlaceholderType {
  PlaceholderType::DoubleDollarMaths(cite.to_owned())
}


fn parse_cite(cite: &str) -> PlaceholderType {
  PlaceholderType::Cite(cite.to_owned())
}

fn parse_full_cite(cite: &str) -> PlaceholderType {
  PlaceholderType::FullCite(cite.to_owned())
}

fn parse_at_cite(cite: &str) -> PlaceholderType {
  PlaceholderType::AtCite(cite.to_owned())
}

#[derive(PartialEq, Debug, Clone)]
pub enum PlaceholderType {
  Cite(String),
  FullCite(String),
  AtCite(String),
  DoubleDollarMaths(String),
  DollarMaths(String)
}

#[derive(Debug, Clone)]
pub(crate) struct Placeholder {
  pub start_index: usize,
  pub end_index: usize,
  pub placeholder_type: PlaceholderType,
}

impl PartialOrd for Placeholder {
  fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
      if self == other {
        return Some(Ordering::Equal);
      } else if self.start_index < other.start_index {
        return Some(Ordering::Less);
      } else if self.start_index > other.start_index {
        return Some(Ordering::Greater);
      };
    None
  }
}

impl PartialEq for Placeholder {
  fn eq(&self, other: &Self) -> bool {
    self.start_index == other.start_index
      && self.end_index == other.end_index
  }
}

impl Placeholder {
    pub(crate) fn from_capture(cap: Captures) -> Option<Placeholder> {
      let placeholder_type = match (cap.get(0), cap.get(1), cap.get(2),) {
          (_, Some(typ), Some(rest)) => {
              let arg = rest.as_str().trim();

              match typ.as_str(){
                  "cite"      => Some(parse_cite(arg)),
                  "fullcite"  => Some(parse_full_cite(arg)),
                  "@@"        => Some(parse_at_cite(arg)),
                  "$$"        => Some(parse_double_dollar(arg)),
                  "$"         => Some(parse_dollar(arg)),
                  _           => None,
              }
          }
          _ => None,
      };

      placeholder_type.and_then(|plh_type| {
          cap.get(0).map(|mat| Placeholder {
              start_index: mat.start(),
              end_index: mat.end(),
              placeholder_type: plh_type
          })
      })
  }

  pub(crate) fn render(
    &self,
    root_path: String,
    bibliography: &Bibliography, 
    references_tpl: &String
  ) -> String {

    match self.placeholder_type {
      PlaceholderType::Cite(ref cite_list)
      | PlaceholderType::AtCite(ref cite_list) => {
        let mut render_text: String = "".to_string();
        for cite in cite_list.split(','){
          if !render_text.is_empty() {
            render_text.push_str(", ");
          }
          if let Some(entry) = bibliography.get(cite) {
            render_text.push_str("\\([");
            render_text.push_str(&BibItem::from_bib_entry(entry).get_citation());
            render_text.push_str("](");
            render_text.push_str(&root_path);
            render_text.push_str("bibliography.html#");
            render_text.push_str(cite);
            render_text.push_str(")\\)");
          } else {
            render_text.push_str("\\[Unknown bib ref: ");
            render_text.push_str(cite);
            render_text.push_str("\\]");
          }
        }
        render_text
      },
      PlaceholderType::FullCite(ref cite) => {
        if let Some(entry) = bibliography.get(cite) {
          let mut handlebars = Handlebars::new();
          handlebars
            .register_template_string("references", references_tpl)
            .unwrap();
            handlebars.render("references", &BibItem::from_bib_entry(entry)).unwrap()
        } else {
            format!("\\[Unknown bib ref: {}\\]", cite)
        }
      },
      PlaceholderType::DoubleDollarMaths(ref math_text) => {
        format!("\\\\[ {} \\\\]", math_text)
      },
      PlaceholderType::DollarMaths(ref math_text) => {
        format!("\\\\( {} \\\\)", math_text)
      },
    }
  }
}

pub(crate)  struct PlaceholderIter<'a>(CaptureMatches<'a, 'a>);

impl<'a> Iterator for PlaceholderIter<'a> {
  type Item = Placeholder;
  fn next(&mut self) -> Option<Placeholder> {
      for cap in &mut self.0 {
        if let Ok(cap) = cap {
          if let Some(plh) = Placeholder::from_capture(cap) {
            return Some(plh);
        }
        };
      }
      None
  }
}

pub(crate) fn find_placeholders<'a>(contents: &'a str) -> PlaceholderIter<'a> {
  // lazily compute following regex
  // r"\\\{\{#.*\}\}|\{\{#([a-zA-Z0-9]+)\s*([a-zA-Z0-9_.\-:/\\\s]+)\}\}")?;
  lazy_static! {
      static ref RE: Regex = Regex::new(
          r"(?x)                       # insignificant whitespace mode
          \\\{\{\#.*\}\}               # match escaped placeholder
          |                            # or
          \{\{\s*                      # placeholder opening parens and whitespace
          \#([a-zA-Z0-9_]+)            # placeholder type
          \s+                          # separating whitespace
          ([a-zA-Z0-9\s_,\.\-:\/\\\+]+)   # placeholder target path and space separated properties
          \s*\}\}                      # whitespace and placeholder closing parens"
      )
      .unwrap();
  }
  PlaceholderIter(RE.captures_iter(contents))
}


const AT_REF_PATTERN: &str = r##"(@@)([^\[\]\s\.,;"#'()={}%]+)"##;
pub(crate) fn find_at_placeholders<'a>(contents: &'a str) -> PlaceholderIter<'a>  {
    lazy_static! {
        static ref REF_REGEX: Regex = Regex::new(AT_REF_PATTERN).unwrap();
    }
    PlaceholderIter(REF_REGEX.captures_iter(contents))
}

const DOLLAR_MATH_PATTERN: &str = r##"(?<!\$)(\$)([^$]+)\$(?!\$)"##;
pub(crate) fn find_dollar_placeholders<'a>(contents: &'a str) -> PlaceholderIter<'a>  {
    lazy_static! {
        static ref DOLLAR_MATH: Regex = Regex::new(DOLLAR_MATH_PATTERN).unwrap();
    }
    PlaceholderIter(DOLLAR_MATH.captures_iter(contents))
}

const DOUBLE_DOLLAR_MATH_PATTERN: &str = r##"(?<!\$)(\$\$)([^$]+)\$\$(?!\$)"##;
pub(crate) fn find_double_dollar_placeholders<'a>(contents: &'a str) -> PlaceholderIter<'a>  {
    lazy_static! {
        static ref DOUBLE_DOLLAR_MATH: Regex = Regex::new(DOUBLE_DOLLAR_MATH_PATTERN).unwrap();
    }
    PlaceholderIter(DOUBLE_DOLLAR_MATH.captures_iter(contents))
}


